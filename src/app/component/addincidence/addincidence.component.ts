import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular'
import { ModalAddIncidencePage } from '../../pages/modal-add-incidence/modal-add-incidence.page';

@Component({
  selector: 'app-addincidence',
  templateUrl: './addincidence.component.html',
  styleUrls: ['./addincidence.component.scss'],
})
export class AddincidenceComponent implements OnInit {

  constructor(private modalController: ModalController) { }

  ngOnInit() {

  }

  async openModal(){
    const modal = await this.modalController.create({
      component: ModalAddIncidencePage,
      componentProps: {

      }
    });
    modal.present();
  }

}
