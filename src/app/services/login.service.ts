import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    // 'Access-Control-Allow-Origin': '*'
  })
};

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  getUsers(){

    let promise = new Promise((resolve, reject) => {
      this.http.get('http://134.209.255.201/users/').toPromise().then(res=> {
        //console.log(res);
        resolve(res);
      },
      msg => {
        console.log(msg); //error ocurrido
        reject(); //Promesa fallida
      }
      );
    });
    return promise;

  }

  token(data){
    let promise = new Promise((resolve, reject) => {
      // this.http.post('http://134.209.255.201/users/token', data, httpOptions).toPromise().then(res=> {
        this.http.post('http://localhost/back-cakephp-angular/users/token', data, httpOptions).toPromise().then(res=> {
        console.log(res);
        resolve(res);
      },
      msg => {
        console.log(msg); //error ocurrido
        reject(); //Promesa fallida
      }
      );
    });
    return promise;
  }

}
