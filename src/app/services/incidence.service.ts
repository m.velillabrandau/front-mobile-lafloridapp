import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions2 = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*',
    'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE5fQ.fuMo9HoF6pG7vze_HH7tScq2NssuuLmq8T8qmgFVfq8'
  })
};

@Injectable({
  providedIn: 'root'
})
export class IncidenceService {

  constructor(private http: HttpClient) { }

  addIncidence(data){
    let promise = new Promise((resolve, reject) => {
      // this.http.post('http://134.209.255.201/incidents', data, httpOptions2).toPromise().then(res=> {
        this.http.post('http://localhost/back-cakephp-angular/incidents', data, httpOptions2).toPromise().then(res=> {
        //console.log(res);
        resolve(res);
      },
      msg => {
        console.log(msg); //error ocurrido
        reject(); //Promesa fallida
      }
      );
    });
    return promise;
  }
}
