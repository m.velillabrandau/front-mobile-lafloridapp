import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular'
import { ModalAddIncidencePage } from '../modal-add-incidence/modal-add-incidence.page';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  constructor(private modalController: ModalController) { }

  ngOnInit() {
    
  }

  async openModal(){
    const modal = await this.modalController.create({
      component: ModalAddIncidencePage,
      componentProps: {

      }
    });
    modal.present();
  }

}
