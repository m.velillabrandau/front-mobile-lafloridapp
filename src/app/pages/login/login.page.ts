import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { Observable } from 'rxjs';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  data;
  dataUser = {}

  constructor(private router: Router, private loginServices: LoginService) { }

  ngOnInit() {
    console.log("INICIO");
    // this.loginServices.getUsers().then(res => {
    //   console.log(res['users']);
    // }).catch(err=> {
    //   //err
    // });
    //console.log("DATA:", data);
  }

  login(){
    console.log(this.dataUser);
    this.loginServices.token(this.dataUser).then(res => {
      console.log(res['success']);
      if(res['success'] == true){
        console.log(res);
        this.router.navigate(['/dashboard']);
      }
    }).catch(err=> {
      //err
    });
  }

}
