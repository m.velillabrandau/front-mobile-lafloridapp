import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { NavParams } from '@ionic/angular';
import { ModalController } from '@ionic/angular'
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { LoadingController } from '@ionic/angular'
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { File, FileEntry } from '@ionic-native/file/ngx';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';
import { Storage } from '@ionic/storage';
import { HttpClient } from '@angular/common/http';
import { ActionSheetController, ToastController, Platform} from '@ionic/angular';
import { FilePath } from '@ionic-native/file-path/ngx';
import { IncidenceService } from '../../services/incidence.service';
import { Router, ActivatedRoute } from "@angular/router";

import { finalize } from 'rxjs/operators';
 
const STORAGE_KEY = 'my_images';

declare var google;



@Component({
  selector: 'app-modal-add-incidence',
  templateUrl: './modal-add-incidence.page.html',
  styleUrls: ['./modal-add-incidence.page.scss'],
})
export class ModalAddIncidencePage implements OnInit {

  dataIncidence = {};
  lat: number;
  lng: number;
  images = [];


  constructor(
    private navParams: NavParams, 
    private modalController: ModalController, 
    private geolocation: Geolocation,
    private loadCtrl: LoadingController,
    private camera: Camera, 
    private file: File, 
    private http: HttpClient, 
    private webview: WebView,
    private actionSheetController: ActionSheetController, 
    private toastController: ToastController,
    private storage: Storage, 
    private plt: Platform, 
    private loadingController: LoadingController,
    private ref: ChangeDetectorRef, 
    private filePath: FilePath,
    private router: Router, 
    private incidenceService: IncidenceService
    ) { 

  }

  closeModal(){
    this.modalController.dismiss();
  }

  ngOnInit() {
    this.loadMap();
    this.plt.ready().then(() => {
      this.loadStoredImages();
    });

  }

  async loadMap(){

    const loading = await this.loadCtrl.create();
    loading.present();

    const rta = await this.geolocation.getCurrentPosition();
    const myLatLng = {
      lat: rta.coords.latitude,
      lng: rta.coords.longitude
    }

    this.lat = myLatLng.lat;
    this.lng = myLatLng.lng;

    console.log(myLatLng);
    const mapEle: HTMLElement = document.getElementById('map');
    const map = new google.maps.Map(mapEle,{
      center: myLatLng,
      zoom: 12,
      streetViewControl: false,
      disableDefaultUI: true
    });
    google.maps.event.addListenerOnce(map, 'idle', () =>{
      loading.dismiss();
      const marker = new google.maps.Marker({
        position: {
          lat: myLatLng.lat,
          lng: myLatLng.lng
        },
        map: map,
        title: '¡ Estoy aca !'
      })
    });
  }

  loadStoredImages() {
    this.storage.get(STORAGE_KEY).then(images => {
      if (images) {
        let arr = JSON.parse(images);
        this.images = [];
        for (let img of arr) {
          let filePath = this.file.dataDirectory + img;
          let resPath = this.pathForImage(filePath);
          this.images.push({ name: img, path: resPath, filePath: filePath });
        }
      }
    });
  }
 
  pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      let converted = this.webview.convertFileSrc(img);
      return converted;
    }
  }
 
  async presentToast(text) {
    const toast = await this.toastController.create({
        message: text,
        position: 'bottom',
        duration: 3000
    });
    toast.present();
  }

  async selectImage() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Modify your album',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
              this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
      },
      {
          text: 'Use Camera',
          handler: () => {
              this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
      },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });

    await actionSheet.present();
  }

  takePicture(sourceType: PictureSourceType) {
      var options: CameraOptions = {
          quality: 100,
          sourceType: sourceType,
          saveToPhotoAlbum: false,
          correctOrientation: true
      };
  
      this.camera.getPicture(options).then(imagePath => {
          if (this.plt.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
              this.filePath.resolveNativePath(imagePath)
                  .then(filePath => {
                      let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                      let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                      this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
                  });
          } else {
              var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
              var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
              this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
          }
      });
  
  }

  createFileName() {
    var d = new Date(),
        n = d.getTime(),
        newFileName = n + ".jpg";
    return newFileName;
  }

  copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
        this.updateStoredImages(newFileName);
    }, error => {
        this.presentToast('Error while storing file.');
    });
  }

  updateStoredImages(name) {
    this.storage.get(STORAGE_KEY).then(images => {
        let arr = JSON.parse(images);
        if (!arr) {
            let newImages = [name];
            this.storage.set(STORAGE_KEY, JSON.stringify(newImages));
        } else {
            arr.push(name);
            this.storage.set(STORAGE_KEY, JSON.stringify(arr));
        }

        let filePath = this.file.dataDirectory + name;
        let resPath = this.pathForImage(filePath);

        let newEntry = {
            name: name,
            path: resPath,
            filePath: filePath
        };

        this.images = [newEntry, ...this.images];
        this.ref.detectChanges(); // trigger change detection cycle
    });
  }

  deleteImage(imgEntry, position) {
    this.images.splice(position, 1);

    this.storage.get(STORAGE_KEY).then(images => {
        let arr = JSON.parse(images);
        let filtered = arr.filter(name => name != imgEntry.name);
        this.storage.set(STORAGE_KEY, JSON.stringify(filtered));

        var correctPath = imgEntry.filePath.substr(0, imgEntry.filePath.lastIndexOf('/') + 1);

        this.file.removeFile(correctPath, imgEntry.name).then(res => {
            this.presentToast('File removed.');
        });
    });
  }

  addIncidence() {

    console.log(this.images[0]);

    this.file.resolveLocalFilesystemUrl(this.images[0].filePath)
        .then(entry => {
            ( < FileEntry > entry).file(file => {
              const reader = new FileReader();
              reader.onloadend = () => {
                  const formData = new FormData();
                  const imgBlob = new Blob([reader.result], {
                      type: file.type
                  });
                  formData.append('file', imgBlob, file.name);
                  console.log("BLOB:", imgBlob);
                  console.log("FORMDATA: ",formData);

                  // console.log(this.dataIncidence);
                  // console.log("LAT: ", this.lat );
                  // console.log("LNG: ", this.lng );
                  this.dataIncidence['lat'] = this.lat;
                  this.dataIncidence['lng'] = this.lng;
                  console.log("ENVIO:", this.dataIncidence);
                  this.dataIncidence['img'] = formData;
                  console.log("ENVIO2:", this.dataIncidence);
                  //this.uploadImageData(formData);
                  this.uploadData(this.dataIncidence);
              };
              reader.readAsArrayBuffer(file);
            })
        })
        .catch(err => {
            this.presentToast('Error while reading file.');
        });

  }

  addIncidence2() {


    // console.log(this.dataIncidence);
    // console.log("LAT: ", this.lat );
    // console.log("LNG: ", this.lng );
    this.dataIncidence['lat'] = this.lat;
    this.dataIncidence['lng'] = this.lng;
    console.log("ENVIO:", this.dataIncidence);
    //this.uploadImageData(formData);
    this.uploadData(this.dataIncidence);



  }

  readFile(file: any) {
    const reader = new FileReader();
    reader.onloadend = () => {
        const formData = new FormData();
        const imgBlob = new Blob([reader.result], {
            type: file.type
        });
        formData.append('file', imgBlob, file.name);
        console.log("BLOB:", imgBlob);
        console.log("FORMDATA: ",formData);
        //this.uploadImageData(formData);
    };
    reader.readAsArrayBuffer(file);
  }

  // async uploadImageDataOr(formData: FormData) {

  //   const loading = await this.loadingController.create({
  //       // content: 'Uploading image...',
  //   });
  //   await loading.present();

  //   this.http.post("http://localhost:8888/upload.php", formData)
  //       .pipe(
  //           finalize(() => {
  //               loading.dismiss();
  //           })
  //       )
  //       .subscribe(res => {
  //           if (res['success']) {
  //               this.presentToast('File upload complete.')
  //           } else {
  //               this.presentToast('File upload failed.')
  //           }
  //       });
  // }

  uploadData(data){
    
    
    // console.log("ENVIO2:", this.dataIncidence);
    console.log("EN UPLOAD DATA");

    this.incidenceService.addIncidence(data).then(res => {
      console.log("RESPUESTA ADD INCIDENCE: ",res);
    }).catch(err=> {
      //err
    });
    
  }

}
