import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAddIncidencePage } from './modal-add-incidence.page';

describe('ModalAddIncidencePage', () => {
  let component: ModalAddIncidencePage;
  let fixture: ComponentFixture<ModalAddIncidencePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAddIncidencePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAddIncidencePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
