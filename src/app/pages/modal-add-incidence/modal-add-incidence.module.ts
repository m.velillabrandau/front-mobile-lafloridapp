import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ModalAddIncidencePage } from './modal-add-incidence.page';

const routes: Routes = [
  {
    path: '',
    component: ModalAddIncidencePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ModalAddIncidencePage]
})
export class ModalAddIncidencePageModule {}
